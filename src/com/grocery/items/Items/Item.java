package com.grocery.items.Items;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/5/13
 * Time: 9:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class Item {

    private int itemId;
    private String itemName;
    private int quantity;
    private String measure;
    private String dateEntered;
    private String dateUpdated;

    /**
     *
     * @return
     */
    public String getItemName() {
        return itemName;
    }

    /**
     *
     * @param itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     *
     * @return
     */
    public int getItemId() {
        return itemId;
    }

    /**
     *
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     */
    public String getMeasure() {
        return measure;
    }

    /**
     *
     * @param measure
     */
    public void setMeasure(String measure) {
        this.measure = measure;
    }

    /**
     *
     * @return
     */
    public String getDateEntered() {
        return dateEntered;
    }

    /**
     *
     * @param dateEntered
     */
    public void setDateEntered(String dateEntered) {
        this.dateEntered = dateEntered;
    }

    /**
     *
     * @return
     */
    public String getDateUpdated() {
        return dateUpdated;
    }

    /**
     *
     * @param dateUpdated
     */
    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
