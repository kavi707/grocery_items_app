package com.grocery.items.templates;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.grocery.items.Items.Item;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/18/13
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class SingleItemViewActivity extends Activity {

    private int selectedItemId;

    private TextView itemIdText;
    private TextView itemNameText;
    private TextView itemQuantityAndMeasureText;
    private TextView itemEnteredDateText;
    private TextView itemUpdatedDateText;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.single_item_view_layout);

        setUpViews();
    }

    private void setUpViews() {

        Bundle extras = getIntent().getExtras();
        selectedItemId = extras.getInt("SELECTED_ITEM_ID");

        itemIdText = (TextView) findViewById(R.id.itemIdTextView);
        itemNameText = (TextView) findViewById(R.id.itemNameTextView);
        itemQuantityAndMeasureText = (TextView) findViewById(R.id.itemQuantityAndMeasureTextView);
        itemEnteredDateText = (TextView) findViewById(R.id.itemEnteredDateTextView);
        itemUpdatedDateText = (TextView) findViewById(R.id.itemUpdatedDateTextView);

        Item getItem = databaseSQLiteOpenHelper.getItemByItemId(selectedItemId);

        itemIdText.setText("Item ID : " + selectedItemId);
        itemNameText.setText("Item Name : " + getItem.getItemName());
        itemQuantityAndMeasureText.setText(getItem.getQuantity() + " " + getItem.getMeasure());
        itemEnteredDateText.setText(getItem.getDateEntered());
        itemUpdatedDateText.setText(getItem.getDateUpdated());
    }
}
