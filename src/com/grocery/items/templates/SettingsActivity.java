package com.grocery.items.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.grocery.items.R;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/18/13
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class SettingsActivity extends Activity {

    private Button changeEmailButton;
    private Button changePasswordButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.settings_layout);

        setUpViews();
    }

    private void setUpViews() {
        changeEmailButton = (Button) findViewById(R.id.changeEmailButton);
        changePasswordButton = (Button) findViewById(R.id.changePasswordButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        changeEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resetEmailIntent = new Intent(SettingsActivity.this, EmailResetActivity.class);
                startActivity(resetEmailIntent);
            }
        });

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resetPasswordIntent = new Intent(SettingsActivity.this, PasswordResetActivity.class);
                startActivity(resetPasswordIntent);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
