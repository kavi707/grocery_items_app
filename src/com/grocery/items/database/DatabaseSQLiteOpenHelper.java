package com.grocery.items.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.grocery.items.Items.Item;
import com.grocery.items.Users.User;
import com.grocery.items.deals.DailyDeal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Users: kavi
 * Date: 5/5/13
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseSQLiteOpenHelper extends SQLiteOpenHelper {

    private SQLiteDatabase sqLiteGroceryItemsDatabase;

    //database name and version
    public static final String DB_NAME = "grocery_items.sqlite";
    public static final int VERSION = 1;

    //users table and table columns
    public static final String USER_TABLE_NAME = "users";
    public static final String USER_ID = "user_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    //last login detail table
    public static final String LOGIN_DETAIL_TABLE_NAME = "last_login_details";
    public static final String LOGIN_ID = "id";
    public static final String STAY_LOGIN_STATUS = "stay_login_status";
    public static final String LOGOUT_STATUS = "logout_status";
    public static final String LAST_LOGIN_EMAIL = "last_login_email";
    public static final String LAST_LOGIN_PASSWORD = "last_login_password";

    //item table and table columns
    public static final String ITEM_TABLE_NAME = "items";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_NAME = "item_name";
    public static final String QUANTITY = "quantity";
    public static final String MEASURE = "measure";
    public static final String DATE_ENTERED = "date_entered";
    public static final String DATE_UPDATED = "date_updated";

    //daily_deal table and table columns
    public static final String DAILY_DEAL_TABLE_NAME = "daily_deals";
    public static final String DEAl_ID = "deal_id";
    public static final String DEAL_NAME = "deal_name";
    public static final String DEAL_ITEM = "deal_item";
    public static final String DEAL_QTY = "deal_qty";
    public static final String DEAL_MEASURE = "deal_measure";
    public static final String DEAL_DATE = "deal_date";

    public DatabaseSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createUserTable(sqLiteDatabase);
        createLastLoginTable(sqLiteDatabase);
        createItemTable(sqLiteDatabase);
        createDailyDealTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    /**
     * create users table in the database
     * @param sqLiteDatabase
     */
    private void createUserTable(SQLiteDatabase sqLiteDatabase) {

        String createUsersTableQuery = "create table " + USER_TABLE_NAME + " ( " +
                USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                FIRST_NAME + " text, " +
                LAST_NAME + " text, " +
                EMAIL + " text, " +
                PASSWORD + " text " +
                ");";
        sqLiteDatabase.execSQL(createUsersTableQuery);

        ContentValues values = new ContentValues();
        values.put(FIRST_NAME,"admin");
        values.put(LAST_NAME, "admin");
        values.put(EMAIL, "admin@abc.com");
        values.put(PASSWORD, "1234");

        try {
            sqLiteDatabase.insert(USER_TABLE_NAME, null, values);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }

    /**
     *
     * @param sqLiteDatabase
     */
    private void createLastLoginTable(SQLiteDatabase sqLiteDatabase) {

        String createLastLoginTableQuery = "create table " + LOGIN_DETAIL_TABLE_NAME + " ( " +
                LOGIN_ID + " int not null, " +
                STAY_LOGIN_STATUS + " int, " +
                LOGOUT_STATUS + " int, " +
                LAST_LOGIN_EMAIL + " text, " +
                LAST_LOGIN_PASSWORD + " text " +
                ");";
        sqLiteDatabase.execSQL(createLastLoginTableQuery);
    }

    /**
     * create items table in the database
     * @param sqLiteDatabase
     */
    private void createItemTable(SQLiteDatabase sqLiteDatabase) {
        String createItemTableQuery = "create table " + ITEM_TABLE_NAME + " ( " +
                ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                ITEM_NAME + " text, " +
                QUANTITY + " int, " +
                MEASURE + " text, " +
                DATE_ENTERED + " text, " +
                DATE_UPDATED + " text " +
                ");";
        sqLiteDatabase.execSQL(createItemTableQuery);
    }

    private void createDailyDealTable(SQLiteDatabase sqLiteDatabase) {
        String createDailyDealTableQuery = "create table " + DAILY_DEAL_TABLE_NAME + " ( " +
                DEAl_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                DEAL_NAME + " text, " +
                DEAL_ITEM + " text, " +
                DEAL_QTY + " int, " +
                DEAL_MEASURE + " text, " +
                DEAL_DATE + " text " +
                ");";
        sqLiteDatabase.execSQL(createDailyDealTableQuery);
    }


    /**
     * add new user to system
     * @param user
     */
    public boolean addNewUser(User user){

        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        assert(user != null);
        ContentValues values = new ContentValues();
        values.put(FIRST_NAME, user.getFirstName());
        values.put(LAST_NAME, user.getLastName());
        values.put(EMAIL, user.getEmail());
        values.put(PASSWORD, user.getPassword());

        try {
            sqLiteGroceryItemsDatabase.insert(USER_TABLE_NAME, null, values);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }

        return result;
    }

    /**
     * update the selected user
     * @param user
     * @return
     */
    public boolean updateUser(User user){
        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        assert (user != null);
        ContentValues values = new ContentValues();
        values.put(EMAIL, user.getEmail());
        values.put(PASSWORD, user.getPassword());

        try {
            sqLiteGroceryItemsDatabase.update(USER_TABLE_NAME, values, USER_ID + "=" + user.getUserId(), null);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }
        return result;
    }

    /**
     * get user from selected email
     * @param email
     * @return
     */
    public ArrayList<User> getUserFromEmail(String email) {

        ArrayList<User> selectedUsers = new ArrayList<User>();
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        try {
            String grepUserQry = "select * from " + USER_TABLE_NAME + " where " + EMAIL + " = '" + email + "'";
            Cursor userCursor = sqLiteGroceryItemsDatabase.rawQuery(grepUserQry, null);

            userCursor.moveToFirst();
            User user;

            if(!userCursor.isAfterLast()) {

                do {
                    int userId = userCursor.getInt(0);
                    String fName = userCursor.getString(1);
                    String lName = userCursor.getString(2);
                    String getEmail = userCursor.getString(3);
                    String password = userCursor.getString(4);

                    user = new User();
                    user.setUserId(userId);
                    user.setFirstName(fName);
                    user.setLastName(lName);
                    user.setEmail(getEmail);
                    user.setPassword(password);

                    selectedUsers.add(user);
                } while(userCursor.moveToNext());
            }

            userCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return selectedUsers;
    }

    /**
     * delete login details from the database
     */
    public void deleteLoginDetails(){
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();
        try {
            sqLiteGroceryItemsDatabase.delete(LOGIN_DETAIL_TABLE_NAME, LOGIN_ID + "=1",null);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }

    /**
     * add last login detail record to system
     * @param data
     * @return
     */
    public boolean insertLoginDetails(Map<String, String> data) {

        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();
        assert(data != null);
        ContentValues values = new ContentValues();
        values.put(LOGIN_ID, 1);
        values.put(STAY_LOGIN_STATUS, data.get("stayLoginStatus"));
        values.put(LOGOUT_STATUS, data.get("logoutStatus"));
        values.put(LAST_LOGIN_EMAIL, data.get("email"));
        values.put(LAST_LOGIN_PASSWORD, data.get("password"));

        try {
            sqLiteGroceryItemsDatabase.insert(LOGIN_DETAIL_TABLE_NAME, null, values);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }

        return result;
    }

    /**
     * get last login detail record
     * @return
     */
    public Map<String, String> getLoginDetails(){

        Map<String, String> detailsMap = new HashMap<String, String>();
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();
        try {
            String getLoginDetailsQry = "select * from " + LOGIN_DETAIL_TABLE_NAME + " where " + LOGIN_ID + " = 1";
            Cursor loginDetailsCursor = sqLiteGroceryItemsDatabase.rawQuery(getLoginDetailsQry, null);
            loginDetailsCursor.moveToFirst();

            if(!loginDetailsCursor.isAfterLast()){
                do {
                    detailsMap.put("stayLoginStatus", loginDetailsCursor.getString(1));
                    detailsMap.put("logoutStatus", loginDetailsCursor.getString(2));
                    detailsMap.put("email", loginDetailsCursor.getString(3));
                    detailsMap.put("password", loginDetailsCursor.getString(4));
                } while (loginDetailsCursor.moveToNext());
            }
            loginDetailsCursor.close();
        } catch (SQLiteException ex) {
            detailsMap = null;
            throw ex;
        }

        return detailsMap;
    }

    /**
     * select all items in the database
     * @return
     */
    public ArrayList<Item> getAllItems() {

        ArrayList<Item> itemList = new ArrayList<Item>();
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + ITEM_TABLE_NAME;
            Cursor itemCursor = sqLiteGroceryItemsDatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            Item item;

            if(!itemCursor.isAfterLast()) {

                do {
                    int itemId = itemCursor.getInt(0);
                    String itemName = itemCursor.getString(1);
                    int itemQuantity = itemCursor.getInt(2);
                    String itemMeasure = itemCursor.getString(3);
                    String enteredDate = itemCursor.getString(4);
                    String updatedDate = itemCursor.getString(5);

                    item = new Item();
                    item.setItemId(itemId);
                    item.setItemName(itemName);
                    item.setQuantity(itemQuantity);
                    item.setMeasure(itemMeasure);
                    item.setDateEntered(enteredDate);
                    item.setDateUpdated(updatedDate);

                    itemList.add(item);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }

    /**
     * select item from given item id
     * @param itemId
     * @return
     */
    public Item getItemByItemId(int itemId){
        Item selectedItem;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        try {
            String grepItemQry = "select * from " + ITEM_TABLE_NAME + " where " + ITEM_ID + " = " + itemId;
            Cursor selectItemCursor = sqLiteGroceryItemsDatabase.rawQuery(grepItemQry, null);

            selectItemCursor.moveToFirst();
            Item item = new Item();
            if(!selectItemCursor.isAfterLast()) {
                int getItemId = selectItemCursor.getInt(0);
                String itemName = selectItemCursor.getString(1);
                int itemQuantity = selectItemCursor.getInt(2);
                String itemMeasure = selectItemCursor.getString(3);
                String enteredDate = selectItemCursor.getString(4);
                String updatedDate = selectItemCursor.getString(5);

                item.setItemId(getItemId);
                item.setItemName(itemName);
                item.setQuantity(itemQuantity);
                item.setMeasure(itemMeasure);
                item.setDateEntered(enteredDate);
                item.setDateUpdated(updatedDate);
            }
            selectItemCursor.close();

            if (item.getItemId() == itemId) {
                selectedItem = item;
            } else {
                selectedItem = null;
            }
        } catch (SQLiteException ex) {
            throw ex;
        }

        return selectedItem;
    }

    /**
     * add new item to the database
     * @param item
     * @return
     */
    public boolean addNewItem(Item item){
        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        assert (item != null);
        ContentValues values = new ContentValues();
        values.put(ITEM_NAME, item.getItemName());
        values.put(QUANTITY, item.getQuantity());
        values.put(MEASURE, item.getMeasure());
        values.put(DATE_ENTERED, item.getDateEntered());
        values.put(DATE_UPDATED, item.getDateUpdated());

        try {
            sqLiteGroceryItemsDatabase.insert(ITEM_TABLE_NAME, null, values);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }
        return result;
    }

    /**
     * update selected item record
     * @param item
     * @return
     */
    public boolean updateItem(Item item) {
        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        assert (item != null);
        ContentValues values = new ContentValues();
        values.put(ITEM_NAME, item.getItemName());
        values.put(QUANTITY, item.getQuantity());
        values.put(DATE_UPDATED, item.getDateUpdated());

        try {
            sqLiteGroceryItemsDatabase.update(ITEM_TABLE_NAME, values, ITEM_ID + " = " + item.getItemId(), null);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }

        return result;
    }

    /**
     * select all deals
     * @return
     */
    public ArrayList<DailyDeal> getAllDeals() {
        ArrayList<DailyDeal> dealsList = new ArrayList<DailyDeal>();
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        try {
            String grepDealsQry = "select * from " + DAILY_DEAL_TABLE_NAME;
            Cursor dealCursor = sqLiteGroceryItemsDatabase.rawQuery(grepDealsQry, null);

            dealCursor.moveToFirst();
            DailyDeal deal;

            if(!dealCursor.isAfterLast()) {
                do {
                    int dealId = dealCursor.getInt(0);
                    String dealName = dealCursor.getString(1);
                    String dealItem = dealCursor.getString(2);
                    int dealQty = dealCursor.getInt(3);
                    String dealMeasure = dealCursor.getString(4);
                    String dealDate = dealCursor.getString(5);

                    deal = new DailyDeal();
                    deal.setDealId(dealId);
                    deal.setDealName(dealName);
                    deal.setDealItem(dealItem);
                    deal.setDealQty(dealQty);
                    deal.setDealMeasure(dealMeasure);
                    deal.setDealDate(dealDate);

                    dealsList.add(deal);
                } while (dealCursor.moveToNext());
            }
            dealCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return dealsList;
    }


    public DailyDeal getDealByDealId(int dealId) {
        DailyDeal selectedDeal;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        try {
            String grepDealQry = "select * from " + DAILY_DEAL_TABLE_NAME + " where " + DEAl_ID + " = " + dealId;
            Cursor selectDealCursor = sqLiteGroceryItemsDatabase.rawQuery(grepDealQry, null);

            selectDealCursor.moveToFirst();
            DailyDeal deal = new DailyDeal();
            if(!selectDealCursor.isAfterLast()) {
                int getDealId = selectDealCursor.getInt(0);
                String dealName = selectDealCursor.getString(1);
                String dealItemName = selectDealCursor.getString(2);
                int dealQty = selectDealCursor.getInt(3);
                String dealMeasure = selectDealCursor.getString(4);
                String dealDate = selectDealCursor.getString(5);

                deal.setDealId(getDealId);
                deal.setDealName(dealName);
                deal.setDealItem(dealItemName);
                deal.setDealQty(dealQty);
                deal.setDealMeasure(dealMeasure);
                deal.setDealDate(dealDate);
            }
            selectDealCursor.close();

            if(deal.getDealId() == dealId) {
                selectedDeal = deal;
            } else {
                selectedDeal = null;
            }
        } catch (SQLiteException ex) {
            throw ex;
        }

        return selectedDeal;
    }

    /**
     *
     * @param deal
     * @return
     */
    public boolean addNewDeal(DailyDeal deal) {
        boolean result = false;
        sqLiteGroceryItemsDatabase = this.getWritableDatabase();

        assert (deal != null);
        ContentValues values = new ContentValues();
        values.put(DEAL_NAME, deal.getDealName());
        values.put(DEAL_ITEM, deal.getDealItem());
        values.put(DEAL_QTY, deal.getDealQty());
        values.put(DEAL_MEASURE, deal.getDealMeasure());
        values.put(DEAL_DATE, deal.getDealDate());

        try {
            sqLiteGroceryItemsDatabase.insert(DAILY_DEAL_TABLE_NAME, null, values);
            result = true;
        } catch (SQLiteException ex) {
            result = false;
            throw ex;
        }

        return result;
    }

}
